import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/resource/:idResource',
      name: 'Resource',
      props: true,
      component: () => import('./views/Resource.vue')
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('./views/Login.vue'),
      beforeEnter: (to, from, next) => {
        if (!localStorage.getItem('user')) {
          next()
        } else {
          next('/auth')
        }
      }
    },
    {
      path: '/auth',
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('user')) {
          next()
        } else {
          next('/login')
        }
      },
      component: () => import('./views/Auth.vue'),
      children: [
        {
          path: '',
          redirect: { name: 'Dashboard' }
        },
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: () => import('./views/Dashboard.vue')
        },
        {
          path: 'classifiers',
          name: 'Classifier',
          component: () => import('./views/Classifier.vue')
        },
        {
          path: 'new',
          name: 'NewResource',
          component: () => import('./views/FormResource.vue'),
          props: { 
            headerTitle: 'Nuevo recurso'
          }
        },
        {
          path: 'edit/:idResource',
          name: 'EditResource',
          component: () => import('./views/FormResource.vue'),
          props: {
            default: true,
            headerTitle: 'Editar recurso'
          }
        },
        {
          path: 'upload/image/:idResource',
          name: 'UploadImage',
          component: () => import('./views/Upload.vue')
        }
      ]
    }
  ],
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
})

export default router