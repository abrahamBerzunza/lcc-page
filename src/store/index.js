import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    records: [],
    recordsPlain: [],
  },

  getters: {
    getRecordById: (state) => (id) => {
      return state.recordsPlain.find(record => record.item_id === id)
    }
  },

  mutations: {
    ADD_RECORDS (state, records) {
      state.records = records.parser
      state.recordsPlain = records.plain
    }
  },

  actions: {
    setRecords({ commit }, records) {
      commit('ADD_RECORDS', records)
    }
  }
})
