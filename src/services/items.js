import apiClient from './index'

function createItem (item) {
  let bodyFormData = new FormData()
  bodyFormData.append('user_id', item.user_id);
  bodyFormData.append('title', item.title);
  bodyFormData.append('description', item.description);
  bodyFormData.append('body', item.body);
  bodyFormData.append('in_charge', item.in_charge);
  bodyFormData.append('partners', item.partners);
  bodyFormData.append('item_competence_id', item.item_competence_id);
  bodyFormData.append('posting_date', item.posting_date);
  bodyFormData.append('image', item.image);
  bodyFormData.append('item_type_id', item.item_type_id);
  bodyFormData.append('key_words', item.key_words);

  return apiClient.post('/item/create.php', bodyFormData)
}

function editItem (item) {
  let bodyFormData = new FormData()
  bodyFormData.append('item_id', item.id);
  bodyFormData.append('user_id', item.user_id);
  bodyFormData.append('title', item.title);
  bodyFormData.append('description', item.description);
  bodyFormData.append('body', item.body);
  bodyFormData.append('in_charge', item.in_charge);
  bodyFormData.append('partners', item.partners);
  bodyFormData.append('item_competence_id', item.item_competence_id);
  bodyFormData.append('posting_date', item.posting_date);
  bodyFormData.append('item_type_id', item.item_type_id);
  bodyFormData.append('image', item.image);
  return apiClient.post('/item/update.php', bodyFormData)
}

function getLastThree () {
  return apiClient.get('/item/read_last_three.php')
}

function getItems () {
  return apiClient.get('/item/read.php')
}

function getItemById (id) {
  return apiClient.post('/item/read_one.php', {
    item_id: id
  })
}

function deleteItem (id) {
  return apiClient.post('/item/delete.php', {
    item_id: id
  })
}

function getCompetences () {
  return apiClient.post('/item_competence/read.php')
}

function deleteItemCompetence (idCompetence) {
  return apiClient.post('/item_competence/delete.php', {
    item_competence_id: idCompetence
  })
}

function createCompetence (description) {
  let bodyFormData = new FormData()
  bodyFormData.append('description', description);
  return apiClient.post('/item_competence/create.php', bodyFormData)
}

function updateCompetence (idCompetence, description) {
  let bodyFormData = new FormData()
  bodyFormData.append('item_competence_id', idCompetence);
  bodyFormData.append('description', description);

  return apiClient.post('/item_competence/update.php', bodyFormData)
}

export default {
  createItem,
  editItem,
  getLastThree,
  getItems,
  getItemById,
  deleteItem,
  getCompetences,
  deleteItemCompetence,
  createCompetence,
  updateCompetence
}