import axios from 'axios'

export default axios.create({
  baseURL: process.env.VUE_APP_API_URI,
  timeout: 10000,
  headers: {
    'Accept': 'application/json; multipart/form-data',
    'Content-Type': 'multipart/form-data; application/json; charset=UTF-8'
  }
  // proxy: {
  //   host: '187.157.34.34',
  //   port: 8086
  // }
})