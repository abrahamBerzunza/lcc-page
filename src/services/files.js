import apiClient from './index'

function getFiles() {
  return apiClient.get('/file/read.php')
} 

function deleteFile(fileId) {
  return apiClient.post('/file/delete.php', {
    file_id: fileId
  })
}

function createFile(file) {
  let bodyFormData = new FormData()
  bodyFormData.append('item_id', file.item_id);
  bodyFormData.append('title', file.title);
  bodyFormData.append('source', file.source);
  bodyFormData.append('type', 0);
  return apiClient.post('/file/create.php', bodyFormData)
}

export default {
  createFile,
  getFiles,
  deleteFile
}