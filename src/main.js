import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2';
import { ClientTable } from 'vue-tables-2';

import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(VueSweetalert2);
Vue.use(ClientTable, {}, false, 'bootstrap4', 'default');
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
